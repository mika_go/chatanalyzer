if docker network ls | grep backend_net; then
    echo "Network is already exist"
else
    echo "Creating network"
    docker network create --subnet 10.10.10.0/16 backend_net
fi