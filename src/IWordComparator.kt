package com.example

interface IWordComparator {
    fun compareTo(first:String,second:String,coeff:Int):Boolean
}