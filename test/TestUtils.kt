import com.example.module
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline

fun Application.testableModule() {
    intercept(ApplicationCallPipeline.Call) {
        module()
    }
}

//fun Application.testableModuleWithDependencies(random: Random) {
//    intercept(ApplicationCallPipeline.Call) {
//        if (call.request.uri == "/") {
//            call.respondText("Test: 7")
//        }
//    }
//}